﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using SudokuLib;
using Path = System.IO.Path;

namespace Project01
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<String> lines = new List<string>();
        List<Button> btnList = new List<Button>();
        List<ISudoku> sudokus = new List<ISudoku>();
        Button[,] btnarray = new Button[9,9];
        int selecedSudoku;
       
        public MainWindow()
        {         

            InitializeComponent();
            CreateSoduko(5);
            

            for (int i = 1; i <= 5; i++)
            {
                sodukoselected.Items.Add(i);
            }

           


            for (int i = 0; i <= 8; i++)
            {
                for (int y = 0; y <= 8; y++)
                {
                    Button btn = new Button();
                    btn.Name = "btn" + y + i;
                    btn.Content = $"{i}.{y}";
                    btn.Click += btn_Click;
                    btn.Margin = new Thickness(1,1,1,1);
                    btn.HorizontalContentAlignment = HorizontalAlignment.Center;
                    btn.VerticalContentAlignment = VerticalAlignment.Center;
                    Grid.SetRow(btn, i);
                    Grid.SetColumn(btn, y);
                    MainGrid.Children.Add(btn);
                    btnList.Add(btn);
                    btnarray[i, y] = btn;
                }
            }
        }

        public void CreateSoduko(int amount)
        {
            String newText = "";
            String file_Name = Path.Combine(Environment.CurrentDirectory, @"data\", "top1465.txt");
            if (File.Exists(file_Name))
            {
                StreamReader reader = new StreamReader(file_Name);
                String text;
                int counter = amount;
                while (counter != 0)
                {
                    newText = "";
                    text = reader.ReadLine();
                    foreach (Char ch in text.ToCharArray())
                    {
                        if (ch == '.' || ch == '0')
                        {
                            newText += 0;
                        }
                        else
                        {
                            newText += ch;
                        }

                    }
                    var s = SudokuFactory.CreateSudoku(newText);
                    lines.Add(text);
                    sudokus.Add(s);                    
                    counter--;

                }
            }            
        }

        private void populatebtn(int i)
        {
            int count = 0;
            var s = sudokus[i];


            for (int r = 0; r < 9; ++r)
            {
                for (int c = 0; c < 9; ++c)
                {
                    try
                    {
                        btnarray[r, c].Content = s[r, c].ToString();
                    }
                    catch
                    {

                    }                    
                    count++;
                }                
            }
        }

        private String concertSudoku(int i)
        {
            var s = sudokus[i];
            String result = "";


            for (int r = 0; r < 9; ++r)
            {
                for (int c = 0; c < 9; ++c)
                {
                    if (s[r, c].ToString() == "0")
                    {
                        result += ".";
                    }
                    else
                    {
                        result += s[r, c].ToString();
                    }
                }
            }
            return result;
        }

        private void Sodukoselected_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selecedSudoku = int.Parse(sodukoselected.SelectedItem.ToString()) - 1;                     
            populatebtn(selecedSudoku);
            changeFreeAndUsed(sudokus[selecedSudoku]);
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            ISudoku sudoku = sudokus[selecedSudoku];
            indputWindow indput = new indputWindow(btn, sudoku, MainGrid);
            indput.Owner = this;
            indput.Show();
            

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            String file_Name = Path.Combine(Environment.CurrentDirectory, @"data\", "top1465.txt");
            if (File.Exists(file_Name))
            {
                StreamReader reader = new StreamReader(file_Name);
                String text;
                text = reader.ReadToEnd();
                String newtext = text.Replace(lines[selecedSudoku], concertSudoku(selecedSudoku));
                reader.Close();
                System.IO.File.WriteAllText(file_Name, newtext);
            }
        }

        public void changeFreeAndUsed(ISudoku sudoku)
        {
            filledCells.Content = $"Filled cells {sudoku.NumberOfFilledCells()}";
            openCells.Content = $"Open cells {sudoku.NumberOfOpenCells()}";
        }
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {

            this.Height = this.Width;
        }
    }
}


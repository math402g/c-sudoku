﻿using SudokuLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Project01;

namespace Project01
{
    /// <summary>
    /// Interaction logic for indputWindow.xaml
    /// </summary>
    public partial class indputWindow : Window
    {
        Button btn;
        private int column;
        private int row;
        bool possible = false;
        int count;
        Grid grid;
        ISudoku sudoku;
        private int numberSelected;
        public List<Button> btnList { get; set; }
        private List<Byte> possibleNumbers;
        int[] intList = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        public indputWindow(Button btn, ISudoku sudoku, Grid grid)
        {
            InitializeComponent();
            this.count = 0;
            this.btn = btn;
            column = Grid.GetColumn(btn);
            row = Grid.GetRow(btn);
            this.grid = grid;
            this.sudoku = sudoku;
            input.SelectedIndex = int.Parse(btn.Content.ToString());
            input.ItemsSource = intList;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (possible)
            {
                
                Byte oldValue = sudoku[row, column];
            sudoku[row, column] = Byte.Parse(numberSelected.ToString());

            if (sudoku.IsSolvable())
            {
                btn.Content = numberSelected.ToString();
                errorText.Content = "";
                ((MainWindow)this.Owner).changeFreeAndUsed(sudoku);
                this.Close();
            }
            else
            {
                errorText.Content = "Soduku can't be solved with this number";
                sudoku[row, column] = oldValue;
            }
                
            
             
            }
        else
            {
                errorText.Content = "Number is not possible";
                this.count++;
                if (count > 3)
                {
                    String numbers = "Numbers you can use: ";
                    try
                    {
                        foreach (byte b in possibleNumbers)
                        {
                            numbers += b.ToString() + ", ";
                        }


}
                    catch
                    {
                        numbers = "Ingen mulige numre - overvej en anden løsning";
                    }
                    errorText.Content = numbers;
                }
                
            }

        }

        private void Input_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox selected = (ComboBox)sender;
            possibleNumbers = sudoku.PossibleNumbers(row, column);
            if (possibleNumbers != null)
            {
                foreach (byte b in possibleNumbers)
                {  
                    int numberCheck = (int)b;
                    numberSelected = (int)selected.SelectedItem;
                    if (numberSelected == numberCheck)
                    {

                        possible = true;
                    }
                }
            }
           
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ((MainWindow)this.Owner).changeFreeAndUsed(sudoku);
            this.Hide();
            
        }
    }
}
